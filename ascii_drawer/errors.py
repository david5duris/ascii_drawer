class ParsingError(Exception):
    def __init__(self, value):
        self.value = value
    
    def __str__(self):
        return 'Parsing error in {}'.format(self.value)

class OutOfBoundsError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        val_error = 'Out of bounds coordinate {}'.format(self.value)
        reminder = 'x should be in [0,19], y in [0,9]'
        return val_error + ', ' + reminder

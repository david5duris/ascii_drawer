from ascii_drawer.parser import LineParser
from ascii_drawer.drawing_pane import DrawingPane

if __name__ == '__main__':
    line_segments = LineParser().parse()
    drawing_pane = DrawingPane()
    drawing_pane.draw(line_segments)
    drawing_pane.show_drawing()

import sys

def sign(x):
    result = 0
    if x>0:
        result = 1
    elif x<0:
        result = -1
    return result

def get_arg_line():
    return ' '.join(sys.argv[1:])

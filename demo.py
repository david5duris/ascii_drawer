import time

from ascii_drawer.drawing_pane import DrawingPane, WIDTH, HEIGHT
from ascii_drawer.line_segment import LineSegment

def get_next_contour_coord(x, y):
    """ Assuming (x,y) is on the contour of the pane,
        returns the next coordinates (clockwise direction)
    """

    if (x, y)==(0, 0):
        return 1, 0
    if (x, y)==(WIDTH-1, 0):
        return WIDTH-1, 1
    if (x, y)==(WIDTH-1, HEIGHT-1):
        return WIDTH-2, HEIGHT-1
    if (x, y)==(0, HEIGHT-1):
        return 0, HEIGHT-2
    if x==0:
        return x, y-1
    if x==WIDTH-1:
        return x, y+1
    if y==0:
        return x+1, y
    if y==HEIGHT-1:
        return x-1, y

if __name__ == '__main__':
    xa, ya = 0, 0
    xb, yb = WIDTH-1, HEIGHT-1
    for _ in range(76):
        time.sleep(0.1)
        line_segment = LineSegment(xa, ya, xb, yb)
        xa, ya = get_next_contour_coord(xa, ya)
        xb, yb = get_next_contour_coord(xb, yb)
        drawing_pane = DrawingPane()
        drawing_pane.draw([line_segment])
        drawing_pane.show_drawing()

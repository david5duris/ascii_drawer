## Synopsis

Prints line segments on standard output. They are inputted as a list of
coordinates (x,y)-(x,y),(x,y)-(x,y),... and outputted as sequences of 'X'
characters

## Code Example


```
#!python

python3 ascii draw.py '(2,3)-(10,3), (8,0)-(15,3)'
```
 

outputs
                    

```
#!python

        XX
          XX        
            XX      
  XXXXXXXXX   XX    

```


NB: ' ' symbols around the list of line segments are important (for bash not to
misinterpret parentheses)

## Remarks

x coordinates must belong to [0,19], y coordinates to [0,9], raises an error
otherwise

## Demo


```
#!python

python3 demo.py
```


runs a demo :)

## Tests


```
#!python

pytest
```
 

runs coordinates values checks for some diagonal, vertical, horizontal line segments



Enjoy drawing!
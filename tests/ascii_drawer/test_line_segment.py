import sys
sys.path.append('../..')

from ascii_drawer.line_segment import LineSegment

def test_diagonal():
    line_segment = LineSegment(10, 1, 15, 3)
    expected_points = [
                    (10, 1),
                    (11, 1),
                    (12, 2),
                    (13, 2),
                    (14, 3),
                    (15, 3)
                    ]
    assert set(line_segment.get_points()) == set(expected_points)
    line_segment = LineSegment(15, 3, 10, 1)
    assert set(line_segment.get_points()) == set(expected_points)

def test_vertical():
    line_segment = LineSegment(6, 2, 6, 8)
    expected_points = [
                    (6, 2),
                    (6, 3),
                    (6, 4),
                    (6, 5),
                    (6, 6),
                    (6, 7),
                    (6, 8)
                    ]
    assert set(line_segment.get_points()) == set(expected_points)
    line_segment = LineSegment(6, 8, 6, 2)
    assert set(line_segment.get_points()) == set(expected_points)

def test_horizontal():
    line_segment = LineSegment(14, 9, 18, 9)
    expected_points = [
                    (14, 9),
                    (15, 9),
                    (16, 9),
                    (17, 9),
                    (18, 9),
                    ]
    assert set(line_segment.get_points()) == set(expected_points)
    line_segment = LineSegment(18, 9, 14, 9)
    assert set(line_segment.get_points()) == set(expected_points)

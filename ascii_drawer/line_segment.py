from ascii_drawer.util import sign

class LineSegment(object):
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def get_points(self):
        """ Adds x,y points step by step

            x and y are the current coordinates, 
            they are computed from x1,y1 to x2,y2
            
            direction_x and direction_y are 0/+1/-1 
            depending on the direction of the line segment

            arithmetic variables (ari_ prefixed) are updated at each step. 
            They control the behaviour of x and y 
            (i.e. move to next line/column or not)
        """
        x, y = self.x1, self.y1
        points = [(x, y)]
        direction_x = sign(self.x2 - self.x1)
        direction_y = sign(self.y2 - self.y1)
        ari_step_x = abs(self.x2 - self.x1) + 1
        ari_step_y = abs(self.y2 - self.y1) + 1
        ari_x, ari_y = ari_step_x, ari_step_y
        last_ari_x, last_ari_y = 0, 0
        while (x, y) != (self.x2, self.y2):
            if ari_x//ari_step_y > last_ari_x//ari_step_y:
                x += direction_x
            if ari_y//ari_step_x > last_ari_y//ari_step_x:
                y += direction_y
            last_ari_x, last_ari_y = ari_x, ari_y
            ari_x += ari_step_x
            ari_y += ari_step_y
            points.append((x, y))
        return points

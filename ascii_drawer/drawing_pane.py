WIDTH = 20
HEIGHT = 10

INK = 'X'
NO_INK = ' '

class DrawingPane(object):
    """ Array of characters for drawing line segments """

    def __init__(self):
        """ Inits as an empty pane """
        self.lines = [[NO_INK]*WIDTH for _ in range(HEIGHT)]

    def draw(self, line_segments):
        for line_segment in line_segments:
            points = line_segment.get_points()
            for (x, y) in points:
                self._draw_point(x, y)

    def _draw_point(self, x, y):
        self.lines[y][x] = INK

    def show_drawing(self):
        for line in self.lines:
            print(''.join(line))

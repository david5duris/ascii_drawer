import re

from ascii_drawer.errors import OutOfBoundsError, ParsingError
from ascii_drawer.util import get_arg_line
from ascii_drawer.line_segment import LineSegment

class LineParser(object):
    """ Parse command line to return list of LineSegments """
    def __init__(self):
        pass

    def parse(self):
        line_segments = []
        arg_line = get_arg_line()
        line_segment_regex = r'\(\d+,\d+\)-\(\d+,\d+\)'
        line_segments_raw = re.findall(line_segment_regex, arg_line)
        if not line_segments_raw:
            self.print_usage()
            raise ParsingError(arg_line)
        coords_regex = r'\d+'
        for lsr in line_segments_raw:
            coords_raw = re.findall(coords_regex, lsr)
            if len(coords_raw)!=4: # A line segment is defind by 4 coordinates
                raise ParsingError(lsr)
            coords_int = [int(x) for x in coords_raw]
            self._check_oob_errors(coords_int)
            line_segments.append(LineSegment(
                                coords_int[0],
                                coords_int[1],
                                coords_int[2],
                                coords_int[3]
                                ))
        return line_segments

    def _check_oob_errors(self, coords):
        for c in coords:
            if c<0:
                raise OutOfBoundsError(c)
        for x in [coords[0], coords[2]]:
            if x>19:
                raise OutOfBoundsError(x)
        for y in [coords[1], coords[3]]:
            if y>9:
                raise OutOfBoundsError(y)

    def print_usage(self):
        print("USAGE: python3 ascii_draw.py '(x,y)-(x,y),(x,y)-(x,y), ...'")
